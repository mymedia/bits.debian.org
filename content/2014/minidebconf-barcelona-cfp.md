Title: Call for Proposals for the MiniDebConf 2014 Barcelona
Date: 2014-01-15 19:40
Tags: minidebconf, debian women, announce
Slug: minidebconf-barcelona-cfp
Author: Ana Guerrero Lopez
Status: published

Debian Women will hold a MiniDebConf in Barcelona on March 15-16, 2014.
Everyone is invited to both talks and social events, but the speakers
will all be people who identify themselves as female. This is not
a conference about women in Free Software, or women in Debian,
rather a usual Debian Mini-DebConf where all the speakers are
women.

Debian Women invites submissions of proposals for papers,
presentations, discussion sessions and tutorials for the
event. Submissions are not limited to traditional talks: you
could propose a performance, an art installation, a debate or
anything else. All talks are welcome, whether newbie or very
advanced level. Please, forward this call to potential speakers
and help us make this event a great success!

Please send your proposals to proposals@bcn2014.mini.debconf.org.
Don't forget to include in your message: your name or nick
the title of the event, description, language, and any other
information that might be useful.
Please submit your proposal(s) as soon as possible.

For more information, visit the website of the event:
[http://bcn2014.mini.debconf.org](http://bcn2014.mini.debconf.org)

We hope to see you in Barcelona!
