Title: DebConf20 accueille ses parrains !
Slug: debconf20-welcomes-sponsors
Date: 2020-08-28 10:30
Author: Laura Arjona Reina
Tags: debconf20, debconf, sponsors
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

![DebConf20 logo](|static|/images/dc20-logo-horizontal-diversity.png)

DebConf20 se tient en ligne du 23 août au 29 août 2020. C'est la
vingt-et-unième conférence Debian, et les organisateurs et les participants
travaillent dur ensemble à la création d'événements intéressants et
constructifs.

Nous aimerions saluer chaleureusement les dix-sept parrains de DebConf20 et
vous les présenter.

Nous avons quatre parrains de platine.

Le premier est [**Lenovo**](https://www.lenovo.com).
En tant que leader mondial en technologie, produisant une vaste gamme de
produits connectés, comprenant des smartphones, des tablettes, des
machines de bureau et des stations de travail aussi bien que des
périphériques de réalité augmentée et de réalité virtuelle, des solutions
pour la domotique ou le bureau connecté et les centres de données,
Lenovo a compris combien étaient
essentiels les plateformes et systèmes ouverts pour un monde connecté.

Le parrain suivant est [**Infomaniak**](https://www.infomaniak.com/en).
Infomaniak est la plus grande compagnie suisse d'hébergement web qui offre
aussi des services de sauvegarde et de stockage, des solutions pour les
organisateurs d'événement et de services de diffusion en direct et de
vidéo à la demande. Elle possède l'intégralité de ses centres de données et
de tous les éléments essentiels pour le fonctionnement des services et
produits qu'elle fournit (à la fois sur le plan matériel et logiciel).

[**Google**](https://google.com) est notre troisième parrain de platine.
Google est l'une des plus grandes entreprises technologiques du monde qui
fournit une large gamme de services relatifs à Internet et de produits tels
que des technologies de publicité en ligne, des outils de recherche, de
l'informatique dans les nuages, des logiciels et du matériel. Google apporte
son soutien à Debian en parrainant la DebConf depuis plus de dix ans, et
est également un [partenaire Debian](https://www.debian.org/partners/).

[**Amazon Web Services (AWS)**](https://aws.amazon.com) est notre quatrième
parrain de platine. 
Amazon Web Services est une des plateformes de nuage les plus complètes et
largement adoptées au monde, proposant plus de 175 services complets issus
de centres de données du monde entier (dans 77 zones de disponibilité dans
24 régions géographiques). Parmi les clients d'AWS sont présentes des jeunes
pousses à croissance rapide, de grandes entreprises et des organisations
majeures du secteur public.

Nos parrains d'or sont Deepin, la fondation Matanel, Collabora, et HRT.

[**Deepin**](https://www.deepin.com/) est une compagnie commerciale chinoise
se concentrant sur le développement et les services pour des systèmes
d'exploitation basés sur Linux. Elle pilote aussi la recherche et le
développement de Deepin, une distribution dérivée de Debian.

[**La fondation Matanel**](http://www.matanel.org) opère en Israël, parce
que sa principale préoccupation est de préserver la cohésion d'une société
et d'une nation rongées par les divisions. La fondation Matanel travaille
également en Europe, en Afrique et en Amérique du Sud.

[**Collabora**](https://www.collabora.com/) est un cabinet-conseil
international fournissant des solutions logicielles libres dans le monde
commercial. En plus d'offrir des solutions à ses clients, les ingénieurs et
les développeurs de Collabora contribuent activement à de nombreux projets
à code source ouvert.

[**Hudson-Trading**](https://www.hudsonrivertrading.com) est une société
dirigée par des mathématiciens, des informaticiens, des statisticiens,
des physiciens et des ingénieurs. Ils effectuent des recherches et des
développements d'algorithmes de négociation automatisée en utilisant des
techniques mathématiques avancées.


Nos parrains d'argent sont :

[**Linux Professional Institute**](https://www.lpi.org/), la norme mondiale
de certification et l’organisation de l’assistance professionnelle destinée
aux professionnels du logiciel libre,
[**Civil Infrastructure Platform**](https://www.cip-project.org/),
un projet collaboratif hébergé par la fondation Linux, construisant une
« couche de base » libre pour les logiciels de qualité industrielle,
[**Ubuntu**](https://www.canonical.com/),
le système d'exploitation fourni par Canonical,
et [**Roche**](https://code4life.roche.com/),
un fournisseur et une compagnie de recherche pharmaceutique internationale
majeure dédiée aux soins de santé personnalisés.


Parrains de bronze :
[**IBM**](https://www.ibm.com/),
[**MySQL**](https://www.mysql.com),
[**Univention**](https://www.univention.com/).


Et enfin, nos parrains de la catégorie Supporteur, [**ISG.EE**](https://isg.ee.ethz.ch/)
et [**Pengwin**](https://www.pengwin.dev/).

Merci à tous nos parrains pour leur soutien !
Leurs contributions permettent à un grand nombre de contributeurs Debian du
monde entier de travailler ensemble, de s'entraider et d'apprendre les uns
des autres pendant DebConf20.

## Participer à DebConf20 en ligne

La vingt-et-unième conférence Debian se tient en ligne du 23 au 29 août du
fait de la COVID-19. Des communications, des discussions, des panneaux et
d'autres activités s'enchaînent de 10 h 00 à 01 h 00 UTC.
Visitez le site internet de DebConf20 à l'adresse
[https://debconf20.debconf.org](https://debconf20.debconf.org)
pour en savoir plus sur le programme complet, pour voir les diffusions en
direct et pour rejoindre les différents canaux de communication afin de
participer à la conférence.
