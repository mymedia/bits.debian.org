Title: De officiële communicatiekanalen van Debian
Slug: official-communication-channels
Date: 2020-03-16 14:00
Author: Laura Arjona Reina, Ana Guerrero Lopez and Donald Norwood
Tags: project, announcement
Lang: nl
Translator: Frans Spiesschaert
Status: published

Af en toe krijgen we in Debian vragen over onze officiële
communicatiekanalen, alsook vragen over de Debian-status van wie eigenaar
is van websites met een gelijkaardige naam.


De hoofdwebsite van Debian,
[www.debian.org](https://www.debian.org),
is ons belangrijkste communicatiemedium. Wie informatie zoekt over actuele
gebeurtenissen en de ontwikkelingsvoortgang in de gemeenschap is mogelijk
geïnteresseerd in de
[Debian Nieuws](https://www.debian.org/News/)-sectie van de website van Debian.

Voor minder formele aankondigingen gebruiken we de officiële Debian blog
[Bits from Debian](https://bits.debian.org),
en voor kortere nieuwsitems de dienst [Debian micronews](https://micronews.debian.org).


Onze officiële nieuwsbrief
[Debian Projectnieuws](https://www.debian.org/News/weekly/)
en alle officiële aankondigingen van nieuwsfeiten of veranderingen in het
project, worden zowel gepost op onze website als in onze officiële
mailinglijsten,
[debian-announce](https://lists.debian.org/debian-announce/) of
[debian-news](https://lists.debian.org/debian-news/).
Posten op die mailinglijsten is niet zomaar toegelaten.


We willen ook van de gelegenheid gebruik maken om toe te lichten hoe het
Debian Project, of in het kort, Debian gestructureerd is.


De structuur van Debian wordt gereguleerd in onze
[Statuten](https://www.debian.org/devel/constitution).
Verantwoordelijken en gedelegeerde leden worden vermeld op de pagina over
onze [Organisatiestructuur](https://www.debian.org/intro/organization).
Andere teams worden vermeld op onze
[Teams](https://wiki.debian.org/Teams)-pagina van de wiki.


De volledige lijst van officiële leden van Debian is te vinden op onze
pagina over [Nieuwe Leden](https://nm.debian.org/members),
waar het lidmaatschapsbeheer plaats vindt. Een ruimere lijst van mensen die
bijdragen aan Debian is te vinden op onze
[Medewerkers](https://contributors.debian.org)-pagina.


Indien u vragen heeft, nodigen we u uit contact op te nemen met het
persteam op [press@debian.org](mailto:press@debian.org).
