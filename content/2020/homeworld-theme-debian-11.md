Title: "Homeworld" will be the default theme for  Debian 11
Slug: homeworld-will-be-the-default-theme-for-debian-11
Date: 2020-11-12 13:30
Author: Jonathan Carter
Artist: Juliette Taka
Tags: bullseye, artwork
Status: published

The theme ["Homeworld"][homeworld-link] by Juliette Taka has been selected
as default theme for Debian 11 'bullseye'. Juliette says that this theme
has been inspired by the Bauhaus movement, an art style born in Germany in
the 20th century.

[![Homeworld wallpaper. Click to see the whole theme proposal](|static|/images/homeworld_wallpaper.png)][homeworld-link]

[![Homeworld debian-installer theme. Click to see the whole theme proposal](|static|/images/homeworld_debian-installer.png)][homeworld-link]

[homeworld-link]: https://wiki.debian.org/DebianArt/Themes/Homeworld

After the Debian Desktop Team made the
[call for proposing themes](https://lists.debian.org/debian-devel-announce/2020/08/msg00002.html),
a total of [eighteen choices](https://wiki.debian.org/DebianDesktop/Artwork/Bullseye)
have been submitted. The desktop artwork poll was open to the public,
and we received 5,613 responses ranking the different choices, of which
Homeworld has been ranked as the winner among them.

This is the third time that a submission by Juliette has won. Juliette is
also the author of the [lines theme](https://wiki.debian.org/DebianArt/Themes/Lines)
that was used in Debian 8 and the
[softWaves theme](https://wiki.debian.org/DebianArt/Themes/softWaves)
that was used in Debian 9.

We'd like to thank all the designers that have participated and have submitted
their excellent work in the form of wallpapers and artwork for Debian 11.

Congratulations, Juliette, and thank you very much for your contribution to Debian!
