Title: Официальные информационные каналы Debian
Slug: official-communication-channels
Date: 2020-03-16 14:00
Author: Laura Arjona Reina, Ana Guerrero Lopez and Donald Norwood
Lang: ru
Translator: Lev Lamberov
Tags: project, announcement
Status: published

Время от времени мы получаем вопросы относительно официальных информационных
каналов Debian и вопросы о статусе тех, кто владеет веб-сайтами со сходными
с Debian именами.

Основным источником информации является главный веб-сайт Debian,
[www.debian.org](https://www.debian.org). Тем, кто ищет информацию о текущих
событиях и прогрессе разработки в нашем сообществе, может быть интересен
раздел [Новости Debian](https://www.debian.org/News/) веб-сайта
Debian.

Для менее формальных сообщений у нас имеется блог Debian
[Bits from Debian](https://bits.debian.org),
а также служба [микроновостей Debian](https://micronews.debian.org)
для более коротких новостей.

Наше официальное информационное письмо
[Новости Проекта Debian](https://www.debian.org/News/weekly/)
и все официальные сообщения о новостях или изменениях в Проекте одновременно публикуются
на нашем веб-сайте и рассылаются через наши официальные списки рассылки
[debian-announce](https://lists.debian.org/debian-announce/) или
[debian-news](https://lists.debian.org/debian-news/).
Публикация сообщений в этих списках рассылки ограничена.

Кроме того, мы хотели бы сообщить, как Проект Debian,
или короче &mdash; Debian, структурирован.

Структура Debian регулируется нашей
[Конституцией](https://www.debian.org/devel/constitution).
Официальные лица и делегированные члены сообщества указаны на нашей странице,
описывающей [организационную структуру](https://www.debian.org/intro/organization).
Остальные команды указаны на вики-странице [Teams](https://wiki.debian.org/Teams).

Полный список официальных участников Debian можно найти на
[странице новых участников](https://nm.debian.org/members),
через которую осуществляется управление членством в Проекте. Более широкий список участников Debian
можно найти на нашей странице [Contributors](https://contributors.debian.org).

Если у вас возникли вопросы, то предлагаем вам связаться с командой по связям
с прессой по адресу 
[press@debian.org](mailto:press@debian.org).
