Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng mười một và mười hai 2019)
Slug: new-developers-2019-12
Date: 2020-01-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Louis-Philippe Véronneau (pollo)
  * Olek Wojnar (olek)
  * Sven Eckelmann (ecsv)
  * Utkarsh Gupta (utkarsh)
  * Robert Haist (rha)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Denis Danilov
  * Joachim Falk
  * Thomas Perret
  * Richard Laager

Xin chúc mừng!

