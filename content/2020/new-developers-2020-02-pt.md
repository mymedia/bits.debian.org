Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (janeiro e fevereiro de 2020)
Slug: new-developers-2020-02
Date: 2020-03-23 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os(As) seguintes colaboradores(as) do projeto se tornaram desenvolvedores(as) Debian nos últimos dois meses:

  * Gard Spreemann (gspr)
  * Jonathan Bustillos (jathan)
  * Scott Talbert (swt2c)

Os(As) seguintes colaboradores(as) do projeto se tornaram mantenedores(as) Debian nos últimos dois meses:

  * Thiago Andrade Marques
  * William Grzybowski
  * Sudip Mukherjee
  * Birger Schacht
  * Michael Robin Crusoe
  * Lars Tangvald
  * Alberto Molina Coballes
  * Emmanuel Arias
  * Hsieh-Tseng Shen
  * Jamie Strandboge

Parabéns a todos(as)!

