Title: Debian 7.0 Wheezy released!
Date: 2013-05-05 4:00
Tags: wheezy
Slug: wheezy-released
Author: Francesca Ciceri
Status: published


![Alt Keep calm and install Wheezy](|static|/images/keepcalm.png)

The new stable version of Debian, codenamed *Wheezy*, is finally here.
Main features of Debian 7.0 Wheezy are [multiarch support](https://www.debian.org/News/2011/20110726b),
specific tools to [deploy private clouds](https://www.debian.org/News/2012/20120425), a greatly improved
installer and a complete set of multimedia codecs and front-ends which remove the need for third-party
repositories.
For a complete list of new features and updated software, take a look at the
[official announcement](https://www.debian.org/News/2013/20130504).

Want to give it a try?
Check out the [live images](http://live.debian.net/files/stable/images/7.0.0/)!

Want to install it?
Choose your favourite [installation media](https://www.debian.org/distrib/) among Blu-ray Discs, DVDs,
CDs and USB sticks.

Already a happy Debian user and you only want to upgrade?
You are just an *apt-get dist-upgrade* away from Wheezy!
Find how, reading the [installation guide](https://www.debian.org/releases/wheezy/installmanual)
and the [release notes](https://www.debian.org/releases/wheezy/releasenotes).

Some useful links:

* [getting Wheezy](https://www.debian.org/distrib/)
* [FAQ about installation media](https://www.debian.org/CD/faq/)
* [installation manual](https://www.debian.org/releases/stable/installmanual)
* [Release Notes](https://www.debian.org/releases/wheezy/releasenotes)
