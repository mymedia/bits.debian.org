Title: Ada Lovelace Day: meet some of the "women behind Debian"!
Date: 2013-10-15 00:01
Tags: ada lovelace day, debian women, diversity
Slug: ada-lovelace-day
Author: Ana Guerrero Lopez and Francesca Ciceri
Status: published

Today is [Ada Lovelace](http://en.wikipedia.org/wiki/Ada_Lovelace) Day:

>*"Ada Lovelace Day is about sharing stories of women - whether engineers,
scientist, technologists or mathematicians - who have inspired you to
become who you are today. The aim is to create new role models for girls
and women in these male-dominated fields by raising the profile of other women in STEM".
[source](http://findingada.com)*

To celebrate, we asked to some of the "women behind Debian" to share their stories with us.
Enjoy!

![Alt Ada Lovelace portrait](|static|/images/ada_lovelace.png)

##Ana Guerrero Lopez (ana)

*Who are you?*

I'm a 30-something years old geek. I'm from Andalusia, Spain but live in France.

*What do you do in Debian?*

I work mostly on my packages, in sponsoring new people's packages and in
this very blog you're reading now. I also maintain an *unofficial* Google+
page about Debian. At $PAID job, I work in an internal Debian distribution
so from time to time, I get the opportunity to contribute back some of the
stuff we do there.

*How and why did you start contributing to Debian?*

I started using Debian around 2003 switching from Mandrake. I was a happy
Debian user when the Debian Women project started in the summer 2004. When
I saw the project announced, I asked myself why I wasn't contributing to
Debian and the rest is history... in a couple of weeks it'll be my 7 years
DD-versary! If for some reason you want a longer reply to this question,
read [here](http://raphaelhertzog.com/2012/02/10/people-behind-debian-ana-beatriz-guerrero-lopez-member-of-the-debian-kde-team/).


##Beatrice

*Who are you?*

I am a PhD student with a degree in Biology. I am a computer fan since
my first C64 and I am a self-taught computer geek wanna-be. And I am a
bug fan - not software bugs, real bugs :)

*What do you do in Debian?*

I work on translations - doing the translation work itself, but also
reviewing other translators' work and helping in coordinating the
translation effort.

*How and why did you start contributing to Debian?*

I started using Linux because I liked the idea of an open source
operative system based on collaboration and I began reviewing open
source software translations. Since my first Linux system was Debian
Potato and I sticked to Debian ever since, it only seemed natural to
focus my translation work on Debian.


##Christine Caulfield

*Who are you?*

My name is Christine Caulfield. My day job is principal software
engineer at Red Hat working on the cluster infrastructure components
corosync & pacemaker. Outside computing I'm a musician and sound
engineer. I play violin with lots of technology attached, and love
avant garde music.

*What do you do in Debian?*

I'm not that active on Debian any more due to pressure of time, and
maturity of the packages I work on. I currently maintain the,
little-used, DECnet userspace packages and the, even less used I
suspect, mopd bootloader. I used to maintain lvm2 for a while but
dropped that a few years ago.

*How and why did you start contributing to Debian?*

My initial reasons for joining Debian were slightly selfish, to find a
home for the DECnet project that I was heavily involved in at the
time. I was a keep Debian user and people wanted a distribution where
the software was easy to set up. DECnet is quite complicated for users
to configure, being a totally independant networking stack to IP
and so OS support is needed. Debian seemed like the logical place to
make this happen.
As mentioned above I got quite involved for a time and maintained
other packages too. I picked up lvm2 because I was on the lvm2 dev
team at work in Red Hat and as it was a new package at that time I
seemed a logical choice.


##Elena Grandi (valhalla)

*Who are you?*

I'm a 30-something years old geek and Free Software enthusiast
from Italy.

*What do you do in Debian?*

I'm currently maintaining a few packages (2 python modules and a python
program) as a sponsored uploader; I'm also slowly looking around
for other things to do (by preference technical, but not limited
to packaging), with the aim to spend more time contributing
to Debian.

*How and why did you start contributing to Debian?*

For a while I had being distro-hopping between "fun" distributions
(the ones that break now and then) on the desktop while using
Debian stable on the home server and in chroots.
I was already doing marginal contributions to those distributions,
where finding stuff that was missing was easy, but my perception
as a stable user was that Debian was already working fine and probably
didn't really need any help.
Then I started to socialize on IRC with some DDs and DMs, and
realized that my perception was superficial and that in reality
there were dark holes in the depths of the archive where Evil
festered and prospered and... ok, sorry, I got carried away :)
Anyway, since I was actually using Debian more and more
I decided to start contributing: I read documentation, I attended
the useful IRC sessions on #debian-women and decided that it was
probably best not to add new stuff, but look for things that
I used and that needed help. Then nothing happened for a while,
because finding stuff that doesn't work *is hard* (at least
on my mostly textual systems).
Then one day I was trying to write a python script that needed
to verify gpg signed messages; it had to run on my Debian server,
so I was trying to use python-pyme and its documentation
was painful to use, while I remembered an earlier attempt
using python-gnupg that was much more pythonic, but not available
in Debian.
In a fit of anger I decided to forgo all of my good intentions and
actually add a new package: I checked the sources for problems,
packaged, sent it to mentors@d-o, got reviews, fixed problems,
resent and finally got sponsored and well, everything started.


##Francesca Ciceri (madamezou)

*Who are you?*

I'm Francesca, a 30-something Italian graduated in Social Sciences.

*What do you do in Debian?*

I'm a (non uploading) Debian Developer since 2011 and have been DPN
editor, press officer, webmaster for www.debian.org and translator for the
Italian l10n team. Recently, due to time constraints, I had to reduce my
involvement and now only work on two things: writing/editing articles for
bits.debian.org together with Ana Guerrero, and creating subtitles for the
DebConf talks, in the DebConf Subs team.

*How and why did you start contributing to Debian?*

Basically thanks to the sudden abundance of free time - due to an health
problem - and the desire to give something back to this wonderful operating
system.  After that, I found out that Debian is not only a great OS but also a
very special community. Today, some of my dearest friends are people I met
through Debian. :)

##Laura Arjona

*Who are you?*

I live in Madrid (Spain), and work as IT Assistant in the Technical
University of Madrid (UPM). I'm married and I have a 4-years-old son.

*What do you do in Debian?*

In 2012 I started to clean spam and to translate Debian web pages into
Spanish. I also follow the work of the web and publicity team, I hope
I'll get more involved there too. And of course, I'm in Debian Women
:)

*How and why did you start contributing to Debian?*

I'm using Debian at work since 2007 (servers), and in my desktops since
2010.
I like very much that it is a community distro and I wanted to
participate. I was already doing translations in other (small) free
software projects, so I began here too. The Debian-Women list, the
planet, and people in identi.ca helped me to learn a lot and feel part
of the community even when I was not contributing yet.

##Mònica Ramírez Arceda (monica)

*Who are you?*

My name is Mònica Ramírez Arceda and I am an enthusiast of free software
and sharing knowledge cultures: for me it's a kind of philosophy of
life. I studied Maths a long time ago but ended up working as a
developer for some years. Now I'm working as an IT teacher.

*What do you do in Debian?*

Debian is a huge project, so you can help in various scopes. Mainly, I
work on [packaging](http://qa.debian.org/developer.php?login=monica@debian.org),
[fixing wnpp bug inconsistencies in BTS](http://qa.debian.org/~bartm/wnpp-rfs-mentors/wnpp-inconsistencies.txt)
and [helping in spam cleaning of the mailing lists](https://lists.debian.org/archive-spam-removals/review/).
But I also enjoy doing some non-technical work from time to time: the project I am just now involved
is organizing, with the rest of Debian Catalan community, a local team
to propose [Barcelona as the venue for a minidebconf where all the
speakers will be women](https://wiki.debian.org/DebianWomen/Projects/MiniDebconf-Women/2014/).

*How and why did you start contributing to Debian?*

In 2000 I discovered Free Software world and I fell deeply in love with
its philosophy. Since then, I've been trying to do my best in different
activities, like spreading the word, giving free courses, helping
collectives and friends in technical stuff (from installing Debian to
developing some helping apps for them)... but two years ago I was
looking forward to join a free software project and I decided to try
Debian, since it has been my first and only distro in my day-to-day life
for about ten years.
So, I wanted to give back Debian all what it had offered to me, but....
I thought I couldn't (hey, Debian is for real hackers, not for you
little ant!), but I started to adopt some orphaned packages, do some QA
uploads, fix some RC bugs, talk with some Debian Developers that helped me
and encouraged me more than I expected, I traveled to my first
Debconf... And one thing takes you to the other, and on March 2012 I
became a DD. Now, I'm glad to see that everything that frightened me is
not so scary :-)
