Title: Nous desenvolupadors i mantenidors de Debian (gener i febrer del 2016)
Slug: new-developers-2016-02
Date: 2016-03-14 21:30
Author: Jean-Pierre Giraud
Translator: Adrià García-Alzórriz
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

  * Otto Kekäläinen (otto)
  * Dariusz Dwornikowski (darek)
  * Daniel Stender (stender)
  * Afif Elghraoui (afif)
  * Victor Seva (vseva)
  * James Cowgill (jcowgill)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

  * Giovani Augusto Ferreira
  * Ondřej Nový
  * Jason Pleau
  * Michael Robin Crusoe
  * Ferenc Wágner
  * Enrico Rossi
  * Christian Seiler
  * Daniel Echeverry
  * Ilias Tsitsimpis
  * James Clarke
  * Luca Boccassi

Enhorabona a tots!
