Title: New Debian Developers and Maintainers (July and August 2016)
Slug: new-developers-2016-08
Date: 2016-09-03 12:00
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two months:

  * Edward John Betts (edward)
  * Holger Wansing (holgerw)
  * Timothy Martin Potter (tpot)
  * Martijn van Brummelen (mvb)
  * Stéphane Blondon (sblondon)
  * Bertrand Marc (bmarc)
  * Jochen Sprickerhof (jspricke)
  * Ben Finney (bignose)
  * Breno Leitao (leitao)
  * Zlatan Todoric (zlatan)
  * Ferenc Wágner (wferi)
  * Matthieu Caneill (matthieucan)
  * Steven Chamberlain (stevenc)

The following contributors were added as Debian Maintainers in the last two months:

  * Jonathan Cristopher Carter
  * Reiner Herrmann
  * Michael Jeanson
  * Jens Reyer
  * Jerome Benoit
  * Frédéric Bonnard
  * Olek Wojnar

Congratulations!




