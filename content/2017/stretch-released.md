Title: Debian 9.0 Stretch has been released!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published

![Alt Stretch has been released](|static|/images/banner_stretch.png)

Let yourself be embraced by the purple rubber toy octopus! We're happy to announce
the release of Debian 9.0, codenamed *Stretch*.

**Want to install it?**
Choose your favourite [installation media](https://www.debian.org/distrib/) among Blu-ray Discs, DVDs,
CDs and USB sticks. Then read the [installation manual](https://www.debian.org/releases/stretch/installmanual).

**Already a happy Debian user and you only want to upgrade?**
You can easily upgrade from your current Debian 8 Jessie installation,
please read the [release notes](https://www.debian.org/releases/stretch/releasenotes).

**Do you want to celebrate the release?**
Share the [banner from this blog](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) in your blog or your website!
