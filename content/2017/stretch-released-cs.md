Title: Debian 9.0 Stretch byl vydán!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: cs
Translator: Miroslav Kure

![Alt Stretch byl vydán](|static|/images/banner_stretch.png)

Nechte se pohltit fialovou gumovou chobotničkou! S radostí oznamujeme
vydání Debianu 9.0, kódovým označením *Stretch*.

**Chcete Stretch nainstalovat?**
Zvolte si [instalační médium](https://www.debian.org/distrib/) z dostupných Blu-ray, DVD,
CD nebo USB. A přečtěte si [instalační příručku](https://www.debian.org/releases/stretch/installmanual).

**Již jste spokojený uživatel Debianu a chcete aktualizovat?**
Svou současnou instalaci Debianu 8 Jessie můžete jednoduše aktualizovat,
stačí postupovat dle [poznámek k vydání](https://www.debian.org/releases/stretch/releasenotes).

**Chcete vydání oslavit?**
Sdílejte na svém blogu nebo webu [tento banner](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png)!
