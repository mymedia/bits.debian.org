Title: Bursary applications for DebConf17 are closing in 48 hours!
Slug: dc17-bursaries
Date: 2017-05-08 22:30
Author: Nicolas Dandrimont for the DebConf Team
Tags: debconf17, debconf
Status: published

This is a final reminder: if you intend to apply for a DebConf17 bursary and
have not yet done so, please proceed as soon as possible.

Bursary applications for DebConf17 will be accepted until May 10th at 23:59 UTC.
Applications submitted after this deadline will not be considered.

You can apply for a bursary when you [register](https://debconf17.debconf.org/register)
for the conference.

Remember that giving a talk is considered towards your bursary; if you have a
submission to make, submit it even if it is only sketched-out. You will be able
to detail it later.

Please make sure to double-check your accommodation choices (dates and venue).
Details about accommodation arrangements can be found on the
[wiki](https://wiki.debconf.org/wiki/DebConf17/Accommodation).

Note: For DebCamp we only have on-site accommodation available. The option
chosen in the registration system will only be for the DebConf period (August 5
to 12).

See you in Montréal!


![DebConf17 logo](|static|/images/800px-Dc17logo.png)

