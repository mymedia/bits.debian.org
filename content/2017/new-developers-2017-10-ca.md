Title: Nous desenvolupadors i mantenidors de Debian (setembre i octubre del 2017)
Slug: new-developers-2017-10
Date: 2017-10-02 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

  * Allison Randal (wendar)
  * Carsten Schoenert (tijuca)
  * Jeremy Bicha (jbicha)
  * Luca Boccassi (bluca)
  * Michael Hudson-Doyle (mwhudson)
  * Elana Hashman (ehashman)


Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

  * Ervin Hegedüs
  * Tom Marble
  * Lukas Schwaighofer
  * Philippe Thierry


Enhorabona a tots!
