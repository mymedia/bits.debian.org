Title: Debian chào đón các thực tập sinh GSoC 2018 và Outreachy
Slug: welcome-gsoc2018-and-outreachy-interns
Date: 2018-05-30 20:45
Author: Laura Arjona Reina
Tags: announce, gsoc, outreachy
Translator: Trần Ngọc Quân
Lang: vi
Status: published

![GSoC logo](|static|/images/gsoc.jpg)

![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

Chúng tôi vui mừng thông báo rằng Debian đã chọn được hai mươi sáu thực tập sinh làm việc với chúng tôi
trong suốt những tháng tới: một người cho [Outreachy][1], và hai mươi lăm người cho [Google Summer of Code][2].

[1]: https://www.outreachy.org/alums/
[2]: https://summerofcode.withgoogle.com/organizations/5166394929315840/?sp-page=3#!

Đây là danh sách các dự án và thực tập sinh sẽ làm việc với chúng:

[Cơ sở dữ liệu lịch cho các cuộc hội thảo và sự kiện xã hội](https://wiki.debian.org/SocialEventAndConferenceCalendars)

* [Doğukan Çelik](https://www.dogukancelik.com)

[Các công cụ Android SDK ở Debian](https://wiki.debian.org/AndroidTools)

* [Saif Abdul Cassim](https://salsa.debian.org/m36-guest) (Các công cụ Android SDK ở Debian, GSoC 2018)
* [Chandramouli Rajagopalan](https://salsa.debian.org/r_chandramouli-guest) (Đóng gói và cập nhật
   các công cụ Android SDK)
* [Umang Parmar](https://salsa.debian.org/darkLord-guest) (Các công cụ Android SDK ở Debian)

[Tự động biên dịch với clang sử dụng OBS](https://wiki.debian.org/SummerOfCode2018/Projects/ClangRebuild)

* [Athos Ribeiro](https://athoscr.me/blog/gsoc2018-1/)

[Tự động đóng gói cho mọi thứ](https://wiki.debian.org/SummerOfCode2018/Projects/AutomaticPackagesForEverything)

* [Alexandre Viau](https://salsa.debian.org/aviau)

[Bấm vào để nổi cửa sổ gọi điện cho Linux Desktop](https://wiki.debian.org/SummerOfCode2018/Projects/ClickToDialPopupWindowForLinuxDesktop)

* [Diellza Shabani](https://wiki.debian.org/DiellzaShabani)
* [Sanjay Prajapat](https://salsa.debian.org/sanjaypra555-guest/)
* [Vishal Gupta](https://salsa.debian.org/comfortablydumb-guest/)

[Thiết kế và thực hiện giải pháp Debian SSO](https://wiki.debian.org/SummerOfCode2018/Projects/NewDebianSSO)

* [Birger Schacht](https://bisco.org/tags/gsoc18/)

[Cải tiến EasyGnuPG](https://wiki.debian.org/SummerOfCode2018/Projects/EasyGnuPG)

* [Divesh Uttamchandani](https://wiki.debian.org/DiveshUttamchandani)

[Rút trích dữ liệu từ hóa đơn dạng PDF cho kế toán tài chính](https://wiki.debian.org/SummerOfCode2018/Projects/ExtractingDataFromPDFInvoicesAndBills)

* [Harshit Joshi](https://wiki.debian.org/HarshitJoshi)

[Phần bổ sung Firefox và Thunderbird cho các thói quen phần mềm tự do](https://wiki.debian.org/SummerOfCode2018/Projects/FirefoxAndThunderbirdPluginFreeSoftwareHabits)

* [Enkelena Haxhija](https://wiki.debian.org/EnkelenaHaxhija)

[Ứng dụng đồ họa cho EasyGnuPG](https://wiki.debian.org/SummerOfCode2018/Projects/EasyGnuPG)

* [Yugesh Ajit Kothari](https://github.com/yugeshk)

[Cải tiến hệ thống theo dõi bản phân phối để hỗ trợ các nhóm Debian tốt hơn](https://wiki.debian.org/SummerOfCode2018/Projects/ImprovingDistro-trackerToBetterSupportDebianTeams)

* [Arthur Del Esposte](https://wiki.debian.org/SummerOfCode2018/StudentApplications/ArthurEsposte)

[Kanban Board cho Hệ thống theo dõi lỗi Debian và máy phục vụ CalDAV](https://wiki.debian.org/SummerOfCode2018/Projects/KanbanBoardForDebianBugTrackerAndCalDAVServer)

* [Chikirou Massiwayne](https://wiki.debian.org/SummerOfCode2018/StudentApplications/ChikirouMassiwayne)

[Cải tiến OwnMailbox](https://wiki.debian.org/SummerOfCode2018/Projects/OwnMailbox)

* [Georgios Pipilis](https://github.com/giorgos314)

[Mồi máy qua mạng P2P bằng BitTorrent](https://wiki.debian.org/SummerOfCode2018/Projects/BootTorrent)

* [Shreyansh Khajanchi](https://github.com/shreyanshk)


[PGP Clean Room Live CD](https://wiki.debian.org/SummerOfCode2018/Projects/PGPCleanRoomKeyManagement)

* [Jacob Adams](https://wiki.debian.org/JacobAdams)

[Chuyển các gói Kali sang Debian](https://wiki.debian.org/SummerOfCode2018/Projects/PortKaliPackages)

* [Samuel Henrique](https://salsa.debian.org/samueloph)

[Đảm bảo chất lượng cho các ứng dụng sinh học bên trong Debian](https://wiki.debian.org/SummerOfCode2018/Projects/QualityAssuranceForBiologicalApplicationsInsideDebian)

* [Liubov Chuprikova](https://salsa.debian.org/liubovch-guest)

[Phân tích ngược để điều khiển bộ ổn nhiệt bức xạ Bluetooth](https://wiki.debian.org/SummerOfCode2018/Projects/RadiatorThermovalveReverseEngineering)

* [Sergio Alberti](https://gitlab.com/sergioalberti)

[Máy phục vụ LTSP ảo](https://wiki.debian.org/SummerOfCode2018/Projects/VirtualLtspServer)

* [Deepanshu Gajbhiye](https://github.com/d78ui98)

[Wizard/GUI hỗ trợ các sinh viên/thực tập sinh nộp đơn và hướng dẫn bắt đầu](https://wiki.debian.org/NewContributorWizard)

* [Elena Gjevukaj](https://wiki.debian.org/ElenaGjevukaj)
* [Minkush Jain](https://wiki.debian.org/MinkushJain)
* [Shashank Kumar](https://wiki.debian.org/ShashankKumar)

Xin chúc mừng và chào đón họ!

Chương trình Google Summer of Code và Outreachy có thể thực hiện ở Debian nhờ
có nỗ lực của những nhà phát triển và đóng góp Debian những người mà dành riêng
một phần thời gian rảnh của họ để kèm các thực tập sinh và các nhiệm vụ outreach.

Hãy gia nhập cùng chúng tôi và giúp đỡ mở rộng Debian! Bạn có thể theo dõi báo cáo từng tuần các thực tập sinh trên
[bó thư debian-outreach][debian-outreach-ml], chát với chúng tôi trên
[kênh IRC của chúng tôi][debian-outreach-irc] hay trên từng bó thư của từng nhóm của dự án.

[debian-outreach-ml]: https://lists.debian.org/debian-outreach/ (debian-outreach TẠI lists.debian.org)
[debian-outreach-irc]: irc://irc.debian.org/#debian-outreach (#debian-outreach trên irc.debian.org)
