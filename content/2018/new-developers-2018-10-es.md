Title: Nuevos desarrolladores y mantenedores de Debian (septiembre y octubre del 2018)
Slug: new-developers-2018-10
Date: 2018-11-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Joseph Herlant (aerostitch)
  * Aurélien Couderc (coucouf)
  * Dylan Aïssi (daissi)
  * Kunal Mehta (legoktm)
  * Ming-ting Yao Wei (mwei)
  * Nicolas Braud-Santoni (nicoo)
  * Pierre-Elliott Bécue (peb)
  * Stephen Gelman (ssgelm)
  * Daniel Echeverry (epsilon)
  * Dmitry Bogatov (kaction)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Sagar Ippalpalli
  * Kurt Kremitzki
  * Michal Arbet
  * Peter Wienemann
  * Alexis Bienvenüe
  * Gard Spreemann

¡Felicidades a todos!

