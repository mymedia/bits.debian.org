Title: Alioth's successors, new collaboration platforms in Debian
Date: 2018-02-28 12:30
Tags: debian, alioth,  salsa
Slug: alioth-successors
Author: Laura Arjona Reina
Status: draft

For many years [Alioth], an instance of first GForge and later [FusionForge], has been used by the Debian Project as home for source code repositories, account management for contributors that are not Debian Developers (yet), access management to Debian machines (via SSH keys) and other services as mailing lists and static web pages.

FusionForge is a complex set of software, and even when Debian was not using it in its full capacity (for example, it also offers wiki and forums but they were rarely used in Debian, and many mailing lists were hosted in lists.debian.org instead), the maintenance of the system has been difficult particularly in the last years, when the Alioth team has lacked human resources.

This, together with the need of face an important migration in any case (Alioth was hosted in a Debian machine running Debian Wheezy, which went [End-Of-Life in May]) and the birth of new kind of collaboration tools in the last years, triggered several discussions about the possibility of deprecating Alioth and start to use a different collaboration platform in Debian.

[Alioth]: https://wiki.debian.org/Alioth
[FussionForge]: https://fusionforge.org
[End-Of-Life next May]: https://wiki.debian.org/LTS

In this blog post we'll try to summarise the outcome of those discussions and the different tools that have been chosen for the services that Alioth provided.

# Source code repositories

## Git repositories

In the last months, [Salsa] has been deployed as the proposed tool to handle Git repositories in Debian. Salsa is an instance of GitLab, using the source code of the Community Edition of the project, and deployed and monitored with Ansible.

Salsa allows authentication to all Debian Developers using their Debian login (although they would need to generate a password using the "recover password" link), and other Debian contributors can register a guest account using its self-service interface.

During the last months more than 10,000 Debian repositories have been moved from anonscm.debian.org to salsa.debian.org. Some others (the obsolete ones) have been removed, and finally some others are available in compressed format in https://alioth-archive.debian.org

## Other version control systems

There has been no global alternative planned for non-git source code repositories.
The amount of projects that were in Alioth using Bazaar, CVS, Dacs, Mercurial or Subversion was low, and in most cases the corresponding teams have migrated those repos to Git (and host them in Salsa) or removed them (because they were old copies of projects already using Git).

# Mailing lists

With the shutdown of the lists.alioth.debian.org service, several different alternatives have been offered, depending on the type of the mailing list or the decision of the corresponding team.

* Some mailing lists have been migrated to the "lists.debian.org" service. These migration have included the list of subscribers and the archive of past messages.

* Some other mailing lists have been removed. This is the case of the "commit" lists, that only stored changes in certain source codes repositories. Now with Salsa it is possible to "watch" repositories to receive notifications, and also receive mail notification for each change in the repo (if you subscribe to the package in tracker.debian.org. Talk to your team if this functionality is not implemented yet.

* Finally, a temporary alioth-lists.debian.net service has been offered for the next two years, for the lists that were not suitable for the above options. The migration has been to a similar mailman installation than the one that was offering Alioth, with config, subscribers and archives all intact. An HTTP redirector is provided so
that existing archive URLs continue to work. The service will be reviewed for viability and usefulness after one release cycle, as the expectation is that over time many lists will find a natural home elsewhere.

# Static web pages


# Account management and SSO


# Shell access


# Redirections


# The Alioth machine

# How can you help

