Title: New Debian Developers and Maintainers (July and August 2018)
Slug: new-developers-2018-08
Date: 2018-09-09 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Translator:
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * William Blough (bblough)
  * Shengjing Zhu (zhsj)
  * Boyuan Yang (byang)
  * Thomas Koch (thk)
  * Xavier Guimard (yadd)
  * Valentin Vidic (vvidic)
  * Mo Zhou (lumin)
  * Ruben Undheim (rubund)
  * Damiel Baumann (daniel)

The following contributors were added as Debian Maintainers in the last two months:

  * Phil Morrell
  * Raúl Benencia
  * Brian T. Smith
  * Iñaki Martin Malerba
  * Hayashi Kentaro
  * Arnaud Rebillout

Congratulations!
