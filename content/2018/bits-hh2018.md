Title: Debian Perl Sprint 2018
Slug: debian-perl-sprint-2018
Date: 2018-06-27 18:40
Author: Dominic Hargreaves
Tags: perl, sprint, hh2018
Status: published


Three members of the [Debian Perl](https://wiki.debian.org/Teams/DebianPerlGroup) team met in Hamburg between May 16 and May 20 2018 as part of the [Mini-DebConf Hamburg](https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg) to continue perl development work for Buster and to work on QA tasks across our 3500+ packages.

The participants had a good time and met other Debian friends. The [sprint](https://wiki.debian.org/Sprints/2018/DebianPerlSprint) was productive:

* 21 bugs were filed or worked on, many uploads were accepted.
* The transition to Perl 5.28 was prepared, and versioned provides were again worked on.
* Several cleanup tasks were performed, especially around the move from Alioth to Salsa in documentation, website, and wiki.
* For src:perl, autopkgtests were enabled, and work on Versioned Provides has been resumed.

The [full report](https://lists.debian.org/debian-perl/2018/06/msg00002.html) was posted to the relevant Debian mailing lists.

The participants would like to thank the Mini-DebConf Hamburg organizers for providing the framework for our sprint, and all donors to the Debian project who helped to cover a large part of our expenses.
