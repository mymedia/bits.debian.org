Title: Debian Ruby team sprint 2015
Slug: ruby-sprint-2015
Date: 2015-05-12 00:01
Author: Antonio Terceiro
Tags: ruby, sprint, paris, irill
Status: published

The Debian Ruby [Ruby team](https://wiki.debian.org/Teams/Ruby) had a first
sprint in 2014. The experience was very positive, and it was decided to do it
again in 2015. Last April, the team once more met at the
[IRILL](http://www.irill.org/) offices, in Paris, France.

The participants worked to improve the quality Ruby packages in Debian,
including fixing release critical and security bugs, improving metadata and
packaging code, and triaging test failures on the [Debian Continuous
Integration](https://ci.debian.net/) service.

The sprint also served to prepare the team infrastructure for the future Debian
9 release:

- the `gem2deb` packaging helper to improve the semi-automated generation of
  Debian source packages from existing standard-compliant Ruby packages from
  [Rubygems](https://rubygems.org/).

- there was also an effort to prepare the switch to Ruby 2.2, the latest stable
  release of the Ruby language which was released after the Debian testing
  suite was already frozen for the Debian 8 release.

![Group photo of sprint participants. Left to right: Christian Hofstaedtler, Tomasz Nitecki, Sebastien Badia and Antonio Terceiro](|static|/images/ruby-sprint-2015.jpg)

Left to right: Christian Hofstaedtler, Tomasz Nitecki, Sebastien Badia and Antonio Terceiro.


A full report with technical details has been
[posted](https://lists.debian.org/debian-ruby/2015/05/msg00024.html) to the
relevant Debian mailing lists.
