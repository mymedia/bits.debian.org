Title: Hewlett-Packard Platinum Sponsor of DebConf15
Slug: hp-platinum-debconf15
Date: 2015-03-25 15:00
Author: Laura Arjona Reina
Tags: debconf15, debconf, sponsors, HP
Status: published

[![HPlogo](|static|/images/logo_hp.png)](http://www.hp.com/)

We are very pleased to announce that [**HP**](http://www.hp.com/) has committed support of DebConf15 as **Platinum sponsor**.

"*The hLinux team is pleased to continue HP's long tradition of
supporting Debian and DebConf*," said Steve Geary, Senior Director at
Hewlett-Packard.

Hewlett-Packard  is one of the largest computer companies in the
world, providing a wide range of products and services, such as
servers, PCs, printers, storage products, network equipment, software,
cloud computing solutions, etc.

Hewlett-Packard has been a long-term development partner of Debian, and provides hardware for port development, Debian mirrors, and other Debian services
(HP hardware donations are listed in the [Debian machines](https://db.debian.org/machines.cgi) page).

With this additional commitment as Platinum Sponsor, HP contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software, helping to strengthen the community who continue to collaborate on their
Debian projects throughout the rest of the year.

Thank you very much, Hewlett-Packard, for your support of DebConf15!

## Become a sponsor too!

DebConf15 is still accepting sponsors. Interested companies and organizations may contact the DebConf team through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf15 website at [http://debconf15.debconf.org](http://debconf15.debconf.org).

