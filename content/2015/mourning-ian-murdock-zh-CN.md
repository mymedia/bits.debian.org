Title: Debian 深切哀悼 Ian Murdock 的离世
Date: 2015-12-30 20:15:00
Tags: ian murdock, in memoriam, 悼念
Lang: zh-CN
Slug: mourning-ian-murdock
Author: Ana Guerrero Lopez, Donald Norwood 和 Paul Tagliamonte
Translator: 聞其詳 (bootingman), Anthony Fok, Anthony Wong
Status: published


![Ian Murdock](|static|/images/ianmurdock.jpg)


怀着一颗沉重的心，Debian 同仁深切哀悼刚刚离我们而去的 Ian Murdock，一位为自由开源软件而矢志不渝的创导者，同时也身为人父、身为人子，更是 Debian 中的 “ian”。

Ian 在 1993 年 8 月创立了 “Debian 计划”，并在同年发布了 Debian 的多个初始版本。从此，Debian 逐步成为了流行世界、让大众受惠的通用操作系统；无论是嵌入式设备还是宇宙中的国际空间站，我们都能看到它的身影。

Ian 所专注的，是要创建一种 “择善固执”、坚持 “做正确的事” 的发行版和社区文化；在道德上如此，在技术上亦然。Debian 每个版本 “必先准备就绪才发布” 的力臻完美，以及 Debian 捍卫 “软件自由” 的坚定立场，都是自由及开源软件世界的黄金标准。

Ian 的 “择善固执”，这份对 “正确的事” 的坚持和奉献，一直引导着他的工作，不管是在 Debian 还是在后来的年月里，一直伴随他朝着最好的未来而奋斗。

Ian 的梦想继续发扬光大，Debian 社区依旧非常活跃，成千上万的开发者们奉献数不尽的日日夜夜，带给世界一个安全可靠的操作系统。

在这个伤痛的时刻，Debian 社区众人谨此向 Ian 的家人寄以哀思和慰问；愿我们的思绪陪伴着他们，一起共度难关。

他的家人也请求大家，在这段艰难时期，留给他们一些隐私空间，我们对此非常尊重，同时也号召大家尊重 Ian 家人的意愿。各位来自 Debian 社区以及广大 Linux 社区的朋友，请将你们的慰问发送到 <span style="white-space: nowrap;">[in-memoriam-ian@debian.org](mailto:in-memoriam-ian@debian.org)，所有唁函将被永远保存。

该邮件地址有效期至 2016 年 1 月底。其后，“Debian 计划” 将会把唁函转交给 Ian 的家属保管。如果得到 Ian 家属的同意，唁函内容将于今年稍后公开。

&nbsp;

----

> 中文版原译者：聞其詳 (bootingman)；黃彦邦 (Anthony Wong)
>
> * [〈Debian 社区发表悼念声明，缅怀创始人 Ian Murdock〉](http://www.linuxstory.org/debian-mourns-the-passing-of-ian-murdock/)——[聞其詳 (bootingman)](http://www.bootingman.org/about/) 译
> * [〈Debian 哀悼 Ian Murdock 離世〉](http://blog.anthonywong.net/2016/01/08/debian-%E5%93%80%E6%82%BC-ian-murdock-%E9%9B%A2%E4%B8%96/)——[黃彦邦 (Anthony Wong)](http://blog.anthonywong.net/) 译
>
> 合并及修订：Debian 中文小组
