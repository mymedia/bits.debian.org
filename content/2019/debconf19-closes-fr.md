Title: Clôture de DebConf 19 à Curitiba et annonce des dates de DebConf 20
Date: 2019-07-27 23:40
Tags: debconf19, announce, debconf20, debconf
Slug: debconf19-closes
Author: Laura Arjona Reina et Donald Norwood
Artist: Aigars Mahinovs
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

[![Photo de groupe de DebConf19 – cliquer pour l'agrandir](|static|/images/debconf19_group_small.jpg)](https://wiki.debian.org/DebConf/19/Photos)


Samedi 27 juillet 2019, la conférence annuelle des développeurs et
contributeurs Debian s'est achevée. Avec plus de 380 participants venus de
50 pays différents, et plus de 145 événements combinant communications,
séances de discussion ou sessions spécialisées, ateliers et activités, 
[DebConf19](https://debconf19.debconf.org) a été un énorme succès.


La conférence a été précédée par le DebCamp annuel, du 14 au 19 juillet
qui a mis l'accent sur des travaux individuels et des rencontres d'équipe
pour une collaboration directe au développement de Debian et a accueilli
un atelier d'empaquetage de trois jours où de nouveaux contributeurs ont
pu se lancer dans l'empaquetage Debian.


La [journée porte ouverte](https://debconf19.debconf.org/news/2019-07-20-open-day/)
s'est tenue le 20 juillet, avec plus de 250 participants qui ont pu
apprécier des présentations et des ateliers intéressant un public plus
large, une bourse à l'emploi avec des stands de plusieurs parrains de
DebConf19 et une « install party » Debian.


La conférence des développeurs Debian proprement dite a débuté le
21 juillet 2019. Conjointement à des séances plénières telles que les
traditionnelles « brèves du chef du projet », aux présentations éclair,
aux démonstrations en direct et l'annonce de la DebConf de l'an prochain
([DebConf20](https://wiki.debian.org/DebConf/20) à Haïfa, en Israël),
diverses sessions se sont tenues relatives à la sortie récente de
Debian 10 Buster et certaines de ses nouvelles caractéristiques, ainsi
que des nouvelles et innovations sur plusieurs sujets et équipes internes
de Debian, des sessions de discussion (BoF) des équipes de langue, des
portages, de l'infrastructure et de la communauté, ainsi que de nombreux
autres événements d'intérêt concernant Debian et le logiciel libre.


Le [programme](https://debconf19.debconf.org/schedule/)
a été mis à jour chaque jour avec des activités planifiées et improvisées
introduites par les participants pendant toute la durée de la conférence.


Pour tous ceux qui n'ont pu participer à la DebConf, la plupart des
communications et des sessions ont été enregistrées pour une diffusion
en direct et les vidéos rendues disponibles sur le
[site web des réunions Debian](https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/).
Presque toutes les sessions ont facilité la participation à distance par
des applications de messagerie IRC ou un éditeur de texte collaboratif
en ligne.


Le site web de [DebConf19](https://debconf19.debconf.org/)
restera actif à fin d'archive et continuera à offrir des liens vers les
présentations et vidéos des communications et des événements.


L'an prochain, [DebConf20](https://wiki.debian.org/DebConf/20) se tiendra
à Haïfa, Israël, du 23 au 29 août 2020. Comme à l'accoutumée, les
organisateurs débuteront les travaux en Israël, avant la DebConf, par un
DebCamp (du 16 au 22 août), avec un accent particulier mis sur le travail
individuel ou en équipe pour améliorer la distribution.


DebConf s'est engagée à offrir un environnement sûr et accueillant pour
tous les participants. Durant la conférence, plusieurs équipes (le
secrétariat, l'équipe d'accueil et l'équipe anti-harcèlement)
étaient disponibles pour offrir aux participants, aussi bien sur place
que distants, la meilleure expérience de la conférence, et trouver des
solutions à tout problème qui aurait pu subvenir.
Voir la [page web à propos du Code de conduite sur le site de DebConf19](https://debconf19.debconf.org/about/coc/)
pour plus de détails à ce sujet.


Debian remercie les nombreux [parrains](https://debconf19.debconf.org/sponsors/)
pour leur engagement dans leur soutien à DebConf19, et en particulier nos
parrains platine
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com/)
et [**Lenovo**](https://www.lenovo.com).


### À propos de Debian

Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est
devenu l'un des plus importants et des plus influents projets à code source
ouvert. Des milliers de volontaires du monde entier travaillent ensemble
pour créer et maintenir les logiciels Debian. Traduite en soixante-dix
langues et prenant en charge un grand nombre de types d'ordinateurs, la
distribution Debian est conçue pour être le _système d'exploitation universel_.

### À propos de DebConf

DebConf est la conférence des développeurs du projet Debian. En plus
d'un programme complet de présentations techniques, sociales ou
organisationnelles, DebConf fournit aux développeurs, aux contributeurs et
à toutes personnes intéressées, une occasion de rencontre et de travail
collaboratif interactif. DebConf a eu lieu depuis 2000 en des endroits du
monde aussi variés que l'Écosse, l'Argentine ou la Bosnie-Herzégovine. Plus
d'information sur DebConf est disponible à l'adresse
[https://debconf.org/](https://debconf.org).

### À propos d'Infomaniak

[**Infomaniak**](https://www.infomaniak.com) est la plus grande compagnie
suisse d'hébergement web qui offre aussi des services de sauvegarde et de
stockage, des solutions pour les organisateurs d'événements et des
services de diffusion en direct et de vidéo à la demande.
Elle possède l'intégralité de ses centres de données et des éléments
critiques pour le fonctionnement des services et produits qu'elle fournit
(à la fois sur le plan matériel et logiciel). 


### À propos de Google

[**Google**](https://google.com/) est l'une des plus grandes entreprises
technologique du monde qui fournit une large gamme de services relatifs
à Internet et de produits tels que des technologies de publicité en
ligne, des outils de recherche, de l'informatique dans les nuages, des
logiciels et du matériel.

Google apporte son soutien à Debian en parrainant la DebConf depuis plus
de dix ans, et est également un partenaire de Debian en parrainant les
composants de l'infrastructure d'intégration continue de
[Salsa](https://salsa.debian.org) au sein de la plateforme Google Cloud.


### À propos de Lenovo

En tant que leader mondial en technologie, produisant une vaste gamme de
produits connectés, comprenant des smartphones, des tablettes, des
machines de bureau et des stations de travail aussi bien que des
périphériques de réalité augmentée et de réalité virtuelle, des solutions
pour la domotique ou le bureau connecté et les centres de données,
[**Lenovo**](https://www.lenovo.com) a compris combien étaient
essentiels les plateformes et systèmes ouverts pour un monde connecté.

### Plus d'informations

Pour plus d'informations, veuillez consulter la page internet de
DebConf19 à l'adresse
[https://debconf19.debconf.org/](https://debconf19.debconf.org/)
ou envoyez un message à <press@debian.org>.
