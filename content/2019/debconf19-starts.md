Title: DebConf19 starts today in Curitiba
Slug: debconf19-starts
Date: 2019-07-21 21:10
Author: Laura Arjona Reina
Tags: debconf19, debconf
Status: published

![DebConf19 logo](|static|/images/800px-Debconf19-horizontal.png)

[DebConf19](https://debconf19.debconf.org/), the 20th annual
Debian Conference, is taking place in Curitiba, Brazil
from from July 21 to 28, 2019.

Debian contributors from all over the world have come together at
[Federal University of Technology - Paraná (UTFPR)](https://debconf19.debconf.org/about/venue/)
in Curitiba, Brazil, to participate and work in a conference exclusively
run by volunteers.

Today the main conference starts with over 350 attendants expected
and 121 activities scheduled,
including 45- and 20-minute talks and team meetings ("BoF"),
workshops, a job fair as well as a variety of other events.

The full schedule at
[https://debconf19.debconf.org/schedule/](https://debconf19.debconf.org/schedule/)
is updated every day, including activities planned ad-hoc
by attendees during the whole conference.

If you want to engage remotely, you can follow the
[**video streaming** available from the DebConf19 website](https://debconf19.debconf.org/)
of the events happening in the three talk rooms:
_Auditório_ (the main auditorium), _Miniauditório_ and _Sala de Videoconferencia_.
Or you can join the conversation about what is happening
in the talk rooms:
  [**#debconf-auditorio**](https://webchat.oftc.net/?channels=#debconf-auditorio),
  [**#debconf-miniauditorio**](https://webchat.oftc.net/?channels=#debconf-miniauditorio) and
  [**#debconf-videoconferencia**](https://webchat.oftc.net/?channels=#debconf-videoconferencia)
(all those channels in the OFTC IRC network).

You can also follow the live coverage of news about DebConf19 on
[https://micronews.debian.org](https://micronews.debian.org) or the @debian
profile in your favorite social network.

DebConf is committed to a safe and welcome environment for all participants.
During the conference, several teams (Front Desk, Welcome team and Anti-Harassment team)
are available to help so both on-site and remote participants get their best experience
in the conference, and find solutions to any issue that may arise.
See the [web page about the Code of Conduct in DebConf19 website](https://debconf19.debconf.org/about/coc/)
for more details on this.


Debian thanks the commitment of numerous [sponsors](https://debconf19.debconf.org/sponsors/)
to support DebConf19, particularly our Platinum Sponsors:
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com/)
and [**Lenovo**](https://www.lenovo.com).
