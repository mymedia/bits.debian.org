Title: Debian 10 Buster est publiée !
Date: 2019-07-07 03:25
Tags: buster
Slug: buster-released
Author: Ana Guerrero Lopez, Laura Arjona Reina and Jean-Pierre Giraud
Artist: Alex Makas
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

[![Alt Buster est publiée](|static|/images/upcoming-buster.png)](https://deb.li/buster)

Vous avez toujours rêvé d'avoir un fidèle animal de compagnie ? Il est là et il
s'appelle Buster ! Nous sommes heureux d'annoncer la publication de Debian 10
*Buster*.

**Vous voulez l'installer ?**
Choisissez votre [support d'installation](https://www.debian.org/distrib/) préféré
puis lisez le [manuel d'installation](https://www.debian.org/releases/buster/installmanual).
Vous pouvez aussi utiliser une image officielle pour l'informatique dématérialisée
directement avec votre fournisseur ou essayer Debian avant de l'installer avec
nos images autonomes.

**Vous êtes déjà un utilisateur heureux de Debian, et vous voulez juste la mettre à niveau ?**
Vous pouvez facilement mettre à niveau votre installation de Debian 9 « Stretch »
en consultant les [notes de publication](https://www.debian.org/releases/buster/releasenotes).

**Vous voulez célébrer la publication ?**
Nous vous offrons quelques [illustrations de Buster](https://wiki.debian.org/DebianArt/Themes/futurePrototype)
que vous pouvez partager ou utiliser comme inspiration pour propres créations.
Suivez la conversation à propos de Buster sur les médias sociaux avec les étiquettes #ReleasingDebianBuster
et #Debian10Buster ou venez nous rencontrer en personne ou en ligne dans une
[fête de publication](https://wiki.debian.org/ReleasePartyBuster) !
