Title: Nous desenvolupadors i mantenidors de Debian (març i abril del 2019)
Slug: new-developers-2019-04
Date: 2019-05-11 16:35
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developersen els darrers dos mesos:

  * Jean-Baptiste Favre (jbfavre)
  * Andrius Merkys (merkys)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Christian Ehrhardt
  * Aniol Marti
  * Utkarsh Gupta
  * Nicolas Schier
  * Stewart Ferguson
  * Hilmar Preusse

Enhorabona a tots!

