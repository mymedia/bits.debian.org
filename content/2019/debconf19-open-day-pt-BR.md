Title: DebConf19 convida você para o Dia Aberto do Debian na Universidade Federal de Tecnologia do Paraná (UTFPR), em Curitiba
Slug: debconf19-open-day
Date: 2019-07-20 18:30
Author: Laura Arjona Reina
Tags: debconf, debconf19, debian
Lang: pt-BR
Translator: Carlos Filho
Status: published

![DebConf19 logo](|static|/images/800px-Debconf19-horizontal.png)

[DebConf](https://debconf19.debconf.org), a conferência anual para contribuidores
Debian e usuários interessados em melhorar o [sistema operacional Debian](https://www.debian.org),
será realizada na Universidade Federal de Tecnologia do Paraná (UTFPR)
em Curitiba, Brasil, de 21 a 28 de julho de 2019.
A conferência é precedida pelo DebCamp de 14 a 19 de julho e o Dia Aberto DebConf19 em 20 de julho.

O Dia Aberto, no sábado, 20 de julho, é destinado ao público em geral.
Eventos de interesse para um público mais amplo serão oferecidos,
desde tópicos específicos do Debian até a maior comunidade de Software Livre
e o movimento criador.

O evento é uma oportunidade perfeita para os usuários interessados conhecerem
a comunidade Debian, para o Debian ampliar sua comunidade
e para os patrocinadores da DebConf aumentarem sua visibilidade.

Menos puramente técnico do que a programação principal da conferência,
os eventos no Open Day cobrirão uma grande variedade de tópicos,
desde questões sociais e culturais até workshops e apresentações ao Debian.

A [programação detalhada dos eventos do Dia Aberto](https://debconf19.debconf.org/schedule/?day=2019-07-20)
inclui eventos em inglês e português. Algumas das palestras são:

* "The metaverse, gaming and the metabolism of cities" por Bernelle Verster
* "O Projeto Debian quer você!" por Paulo Henrique de Lima Santana
* "Protecting Your Web Privacy with Free Software" por Pedro Barcha
* "Bastidores Debian - Entenda como a distribuição funciona" por Joao Eriberto Mota Filho
* "Caninos Loucos: a plataforma nacional de Single Board Computers para IoT" por geonnave
* "Debian na vida de uma Operadora de Telecom" por Marcelo Gondim 
* "Who's afraid of Spectre and Meltdown?" por Alexandre Oliva
* "New to DebConf BoF" por Rhonda D'Vine 

Durante o Dia Aberto, haverá também uma Feira de Empregos com estandes
de nossos vários patrocinadores, um workshop sobre o sistema de controle de
versão do Git e um installfest do Debian, para os participantes que gostariam
de obter ajuda para instalar o Debian em suas máquinas.

Todos são bem vindos para participar. Como o restante da conferência,
a participação é gratuita, mas o registro no [site da DebConf19](https://debconf19.debconf.org/)
é altamente recomendado.

A programação completa dos eventos do Dia Aberto e do restante da conferência está em 
[https://debconf19.debconf.org/schedule](https://debconf19.debconf.org/schedule)
e o streaming de vídeo estará disponível no [site da DebConf19](https://debconf19.debconf.org).

A DebConf está comprometida com um ambiente seguro e bem-vindo para todos os participantes.
Veja o [Código de Conduta da DebConf](https://debconf.org/codeofconduct.shtml)
e o [Código de Conduta Debian](https://www.debian.org/code_of_conduct)
para mais detalhes sobre isso.

O Debian agradece aos inúmeros [patrocinadores](https://debconf19.debconf.org/sponsors/)
por seu compromisso com DebConf19, particularmente seus patrocinadores Platinum:
[Infomaniak](https://www.infomaniak.com),
[Google](https://google.com/)
e [Lenovo](https://www.lenovo.com).
