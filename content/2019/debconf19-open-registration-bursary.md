Title: DebConf19 registration is open!
Date: 2019-03-20 20:30
Tags: debconf, debconf19
Slug: debconf19-open-registration-bursary
Author: Sergio Durigan Junior, Paulo Santana
Artist: DebConf19 Design Team
Lang: en
Status: published

![DebConf19 banner open registration](|static|/images/800px-Debconf19-horizontal-open-registration.png)

Registration for [DebConf19](https://debconf19.debconf.org) is now
open. The event **will take place from July 21st to 28th, 2019 at the
Central campus of Universidade Tecnológica Federal do Paraná - UTFPR,
in Curitiba, Brazil**, and will be preceded by DebCamp, from July
14th to 19th, and an [Open Day](https://debconf19.debconf.org/schedule/openday/)
on the 20th.

[DebConf](https://www.debconf.org) is an event open to everyone, no matter how
you identify yourself or how others perceive you. We want to increase visibility
of our diversity and work towards inclusion at Debian Project, drawing our
attendees from people just starting their Debian journey, to seasoned Debian
Developers or active contributors in different areas like packaging,
translation, documentation, artwork, testing, specialized derivatives, user
support and many other. In other words, all are welcome.

To register for the event, log into the
[registration system](https://debconf19.debconf.org/register/)
and fill out the form.
You will be able to edit and update your registration at any
point. However, in order to help the organisers have a better estimate of
how many people will attend the event, we would appreciate if you could
access the system and confirm (or cancel) your participation in the Conference
as soon as you know if you will be able to come. **The last day to confirm or
cancel is June 14th, 2019 23:59:59 UTC**. If you don't confirm or you
register after this date, you can come to the DebConf19 but we cannot
guarantee availability of accommodation, food and swag (t-shirt, bag…).

For more information about registration, please visit
[Registration Information](https://debconf19.debconf.org/about/registration)

## Bursary for travel, accomodation and meals

In an effort to widen the diversity of DebConf attendees,
the Debian Project allocates a part of the financial resources obtained through
sponsorships to pay for bursaries (travel, accommodation, and/or meals) for
participants who request this support when they register.

As resources are limited, we will examine the requests and decide who will
receive the bursaries. They will be destined:

 * To active Debian contributors.
 * To promote diversity: newcomers to Debian and/or DebConf, especially from
under-represented communities.

Giving a talk, organizing an event or helping during DebConf19 is taken into
account when deciding upon
your bursary, so please mention them in your bursary application.
DebCamp plans can be entered in the usual
[Sprints page at the Debian wiki](https://wiki.debian.org/Sprints).

For more information about bursaries, please visit
[Applying for a Bursary to DebConf](https://debconf19.debconf.org/about/bursaries)

**Attention:** the registration for DebConf19 will be open until
Conference, but the **deadline to apply for bursaries using
the registration form before April 15th, 2019 23:59:59 UTC**.
This deadline is necessary in order to the organisers use time to analyze the requests,
and for successful applicants to prepare for the conference.

To register for the Conference, either with or without a bursary request, please
visit: [https://debconf19.debconf.org/register](https://debconf19.debconf.org/register)

DebConf would not be possible without the generous support of all our
sponsors, especially our Platinum Sponsors
[**Infomaniak**](https://www.infomaniak.com)
and [**Google**](https://www.google.com).
DebConf19 is still accepting sponsors; if you are interested, or think
you know of others who would be willing to help, [please get in touch]!

[please get in touch]: https://debconf19.debconf.org/sponsors/become-a-sponsor/

