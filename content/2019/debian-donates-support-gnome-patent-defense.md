Title: Debian Donates to Support GNOME Patent Defense
Slug: debian-donates-support-gnome-patent-defense
Date: 2019-10-28 17:00
Author: Sam Hartman
Tags: debian, gnome, patent trolls, fundraising, donation
Status: published

Today, the Debian Project pledges to [donate] $5,000 to the GNOME
Foundation in support of their ongoing patent defense.  On October 23,
we [wrote to express our support for GNOME] in an issue that affects the
entire free software community.  Today we make that support tangible.

"*This is bigger than GNOME,*" said Debian Project Leader Sam Hartman.
"*By banding together and demonstrating that the entire free software
community is behind GNOME, we can send a strong message to
non-practicing entities (patent trolls).  When you target anyone in the
free software community, you target all of us.  We will fight, and we
will fight to invalidate your patent.  For us, this is more than money.
This is about our freedom to build and distribute our software.*"

"*We're incredibly grateful to Debian for this kind donation, and also
for their support,*" said Neil McGovern, Executive Director of the
GNOME Foundation. "*It's been heartening to see that when free
software is attacked in this way we all come together on a united
front.*"

If GNOME needs more money later in in this defense, Debian will be there
to support the GNOME Foundation.  We encourage individuals and
organizations to join us and stand strong against patent trolls.

[wrote to express our support for GNOME]: https://bits.debian.org/2019/10/gnome-foundation-defense-patent-troll.html
[donate]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
