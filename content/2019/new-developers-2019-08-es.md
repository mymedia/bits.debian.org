Title: Nuevos desarrolladores y mantenedores de Debian (julio y agosto del 2019)
Slug: new-developers-2019-08
Date: 2019-09-17 17:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Keng-Yu Lin (kengyu)
  * Judit Foglszinger (urbec)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Hans van Kranenburg
  * Scarlett Moore

¡Felicidades a todos!

