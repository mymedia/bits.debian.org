Title: Lenovo Patrocinador Platinum da DebConf19
Slug: lenovo-platinum-debconf19
Date: 2019-05-20 10:00
Author: Laura Arjona Reina
Artist: Lenovo
Tags: debconf19, debconf, sponsors, lenovo
Lang: pt-BR
Translator: Sergio Durigan Junior
Status: published

[![lenovologo](|static|/images/lenovo.png)](https://www.lenovo.com/)

Estamos muito felizes em anunciar que
a [**Lenovo**](https://www.lenovo.com/) comprometeu-se a apoiar
a [DebConf19](https://debconf19.debconf.org) como um **patrocinador
Platinum**.

*"A Lenovo está orgulhosa de apoiar a 20ª Conferência Anual do
Debian."*, disse Egbert Gracias, Gerente Sênior de Desenvolvimento de
Software da Lenovo. *"Nós estamos animados para ver de perto o ótimo
trabalho que está sendo feito pela comunidade, e para conhecermos os
desenvolvedores e voluntários que mantêm o Projeto Debian seguindo em
frente!"*

A Lenovo é uma líder tecnológica global, e fabrica uma gama variada de
produtos, incluindo *smartphones*, *tablets*, *PCs* e *workstations*,
bem como dispositivos de realidade aumentada/virtual, soluções para
casas/escritórios inteligentes e para *datacenters*.

Com esse comprometimento como Patrocinador Platinum, a Lenovo
contribui para tornar possível nossa conferência anual e apoia
diretamente o progresso do Debian e do Software Livre, ajudando a
fortalecer a comunidade, que continua a colaborar com projetos do
Debian durante o restante do ano.

Muito obrigado Lenovo por apoiar a DebConf19!

## Torne-se um patrocinador também!

A DebConf19 ainda está aceitando patrocinadores. Empresas e
organizações interessadas devem entrar em contato com o time da
DebConf através
do [sponsors@debconf.org](mailto:sponsors@debconf.org), e visitar o
website da DebConf19
em [https://debconf19.debconf.org](https://debconf19.debconf.org).
