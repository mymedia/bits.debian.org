Title: DebConf19 avslutas i Curitiba och datum för DebConf20 tillkännages
Date: 2019-07-27 23:40
Tags: debconf19, announce, debconf20, debconf
Slug: debconf19-closes
Author: Laura Arjona Reina and Donald Norwood
Artist: Aigars Mahinovs
Lang: sv
Translator: Andreas Rönnquist
Status: published

[![DebConf19 group photo - click to enlarge](|static|/images/debconf19_group_small.jpg)](https://wiki.debian.org/DebConf/19/Photos)


Idag, lördagen 27 juli 2019, avslutades den årliga Debiankonferensen
för för utvecklare och bidragslämnare. Med mer än 300 deltagare från
50 olika länder, med 145 evenemang inklusive föredrag, diskussionssessioner,
Birds of a Feather-möten (BoF), workshops och aktiviteter, hyllas
[DebConf19](https://debconf19.debconf.org) som en succé.


Konferensen föregicks av det årliga DebCamp som hölls 14 juli till 19 juli
med fokus på individuellt arbete och grupparbeten för personligt
samarbete på utveckling av Debian men är även värd för en 3-dagars
workshop för paketering där ni bidragslämnare påbörjar sitt arbete
med Debianpaketering.


[Open Day](https://debconf19.debconf.org/news/2019-07-20-open-day/)
hölls 20 juli, med mer än 250 besökare som beskådade presentationer och
workshops med bred målgrupp, en jobbmässa med bås från flera av sponsorerna av
DebConf19 samt en Debianinstallationsfest.


Den faktiska konferensen startade söndagen 21 juli 2019. Tillsammas med plenarsessioner
som den traditionella Bits from the DPL, blixtföredrag och livedemos samt
tillkännagivandet av nästa års DebConf
([DebConf20](https://wiki.debian.org/DebConf/20) i Haifa, Israel),
har det varit flera sessioner relaterade till den senaste utgåvan av
Debian 10 Buster och några av dess nya funktioner, nyhetsuppdateringar om flera
projekt och interna rupper inom Debian, diskussionssessioner (BoF's) för språkgrupper,
anpassningar, infrastruktur och gemenskapsgrupper, och många andra intressanta
evenemang om Debian och fri mjukvara.


[Schemat](https://debconf19.debconf.org/schedule/)
har uppdaterats varje dag, inklusive ad-hoc-aktiviteter, introducerade
av deltagare under hela konferensen.


För dom som inte kunde besöka konferensen spelades de flesta föredrag och
sessioner in och liveströmmades, och videos görs tillgängliga på
[arkivwebbsidan för Debianmöten](https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19).
Nästan alla underlättade även fjärrdeltagande via IRC eller kollaborativa
textdokument.


[DebConf19-webbsidan](https://debconf19.debconf.org/)
kommer att vara aktiv för arkiveringsändamål, och kommer att fortsätta
erbjuda länkar till presentationer och videos av föredrag och evenemang.


Nästa år kommer [DebConf20](https://wiki.debian.org/DebConf/20) att
hållas i Haifa, Israel, från 23 augusti till 29 augusti 2020.
Enligt traditionen kommer de lokala organisatörerna att före DebConf sätta upp
DebCamp (16 augusti till 22 augusti), med särskilt fokus på individuellt- och
teamarbete för att förbättra distributionen.

DebConf strävar efter en säker och välkomnande miljö för alla deltagare.
Under konferensen finns flera team (receptions-, välkomstteamet, och
Antimobbingsteamet) tillgängliga för att hjälpa så att deltagare både
på plats och fjärrdeltagare får den bästa erfarenheten av konferensen, och
finner lösningar på alla problem som kan uppkomma.
Se [webbsidan om förhållningsregler på DebConf19-webbplatsen](https://debconf19.debconf.org/about/coc/)
för ytterligare detaljer rörande detta.

Debian tackar för åtagandet från flera [sponsorer](https://debconf19.debconf.org/sponsors/)
för deras stöd för DebConf19, speciellt från Platinasponsorer:
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com/)
och [**Lenovo**](https://www.lenovo.com).


### Om Debian

Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
en stor mängd datortyper, kallar sig Debian det <q>universella 
operativsystemet</q>.

### Om DebConf

DebConf är Debianprojektets utvecklarkonferens. Utöver ett fullt schema med
tekniska, sociala och policyföreläsningar, ger DebConf en möjlighet för
utvecklare, bidragslämnare och andra intresserade att mötas personligen
och jobba närmare tillsammans. Det har ägt rum årligen sedan 2000 på
så vitt skiljda platser som Skottland, Argentina, Bosnien och Hercegovina.
Mer information om DebConf finns tillgänglig på 
<url http://debconf.org>.

### Om Infomaniak

[Infomaniak](https://www.infomaniak.com) är Schweiz största webbhostingföretag,
som även erbjuder backup- och lagringstjänster, lösningar för evenemangsorganisatörer,
liveströmmande och video-on-demand-tjänster.
Det äger helt sina datacenter och alla kritiska element för funktionen av
de tjänster och produkter som företaget erbjuder (både mjukvara och hårdvara).


### Om Google

[Google](https://google.com/) är ett av världens största teknologiföretag,
som tillhandahåller ett brett spektrum av Internet-relaterade tjänster och produkter
så som onlinereklamteknologi, sökning, molntjänster, mjukvara och hårdvara.

Google har supportat Debian genom att sponsra DebConf i mer än tio år,
och är även en Debianpartner som sponsrar delar av 
[Salsa](https://salsa.debian.org)'s continuous integration-infrastruktur
med Google Cloud Plattformen.


### Om Lenovo

Som en globla teknologiledare som tillverkar en bred portfolio av anslutna enheter,
inklusive mobiltelefoner, tablets, PCs och arbetsstationer så väl som AR/VR-enheter,
smarta hem/kontor och datacenterlösningar, förstår [Lenovo](https://www.lenovo.com)
hur kritiska öppna system och plattformar är för en ansluten värld.

### Kontaktinformation

För mer information, besök vänligen Debians webbplats på
[https://debconf19.debconf.org/](https://debconf19.debconf.org/) eller skicka e-post (på 
engelska) till <press@debian.org>.
