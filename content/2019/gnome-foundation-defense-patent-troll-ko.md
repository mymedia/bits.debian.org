Title: 데비안 프로젝트는 특허 괴물을 방어하는 그놈 재단과 입장을 같이 합니다
Slug: gnome-foundation-defense-patent-troll
Date: 2019-10-23 10:00
Author: Ana Guerrero López
Tags: debian, gnome, patent trolls, fundraising
Lang: ko
Status: published

2012년, 데비안 프로젝트는 우리의 [소프트웨어 특허에 대한 입장]을 발표했고,
거기에서 자유 소프트웨어에 대해 위협을 가하는 특허의 위험에 대해 언급했습니다.

최근 그놈 재단(GNOME Foundation)은 자유/오픈소스 개인 사진 관리 소프트웨어인
샷웰(Shotwell)의 특허 위반 혐의에 대한 법적 소송을 겪고 있다고 발표했습니다.

그놈 재단은 특허 시스템의 남용에 대해 자유 소프트웨어 공동체가 대항할 것임을
세상에 분명히 보여주었고, 데비안 프로젝트는 이에 대해 단호하게 그놈 재단과
입장을 같이 합니다.

[이 특허 괴물에 대한 그놈 프로젝트의 대응에 대한 이 블로그 기고]를 읽어
보시고, [그놈 특허 괴물 방어 기금]에 기부하시길 생각해 주십시오.

[소프트웨어 특허에 대한 입장]: https://www.debian.org/legal/patent
[이 특허 괴물에 대한 그놈 프로젝트의 대응에 대한 이 블로그 기고]:  https://www.gnome.org/news/2019/10/gnome-files-defense-against-patent-troll/
[그놈 특허 괴물 방어 기금]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
