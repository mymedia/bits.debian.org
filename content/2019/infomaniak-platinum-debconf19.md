Title: Infomaniak Platinum Sponsor of DebConf19
Slug: infomaniak-platinum-debconf19
Date: 2019-02-21 15:45
Author: Laura Arjona Reina
Artist: Infomaniak
Tags: debconf19, debconf, sponsors, infomaniak
Status: published

[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com/)

We are very pleased to announce that [**Infomaniak**](https://www.infomaniak.com/)
has committed to support [DebConf19](https://debconf19.debconf.org) as a **Platinum sponsor**.

*"Infomaniak is proud to support the annual Debian Developers' Conference"*,
said Marc Oehler, Chief Operating Officer at Infomaniak. *"The vast majority 
of our hostings work using Debian and we share this community's values:
promoting innovation whilst ensuring that security,
transparency and user freedom remains top priority."*

Infomaniak is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical 
to the functioning of the services and products provided by the company 
(both software and hardware). 

With this commitment as Platinum Sponsor,
Infomaniak contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Infomaniak, for your support of DebConf19!

## Become a sponsor too!

DebConf19 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf19 website at [https://debconf19.debconf.org](https://debconf19.debconf.org).
