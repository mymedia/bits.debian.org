Title: MiniDebConf Vaumarcus registration is open!
Date: 2019-08-25 20:30
Tags: minidebconf, vaumarcus
Slug: minidebconf-vaumarcus-open-registration
Author: Adriana Cassia da Costa, Didier Raboud
Lang: en
Status: draft

![MiniDebConfVaumarcuslogo](|static|/images/miniDebConf19Vaumarcus-logo.png)

Registration for [MiniDebConf Vaumarcus](https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus/Registration) is now open. The event **will take place from October 25st to 27th,
2019 at Le Camp, in Vaumarcus, Switzerland**.

[MiniDebConf](https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus)
will be open to anybody interested in Debian, will be two days of presentations,
workshops and hacking.

Come see the fantastic view from the shores of Lake Neuchâtel, in Switzerland!
We're going to have two-and-a-half days of presentations and hacking in this
marvelous venue!

## Registration

Registration is open now, and free, so go add your name and details on
the [Debian wiki](https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus/Registration).

We'll accept registrations until late, but don't wait too much before
making your travel plans! We have you covered with a lot of attendee
information already on the [wiki page](https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus).

## Sponsorship

MiniDebConf Vaumarcus is still accepting sponsors; if you are interested, or
think you know of others who would be willing to help,
[please get in touch](board@debian.ch)!

## Getting in touch

We gather on the IRC channel #debian.ch on [irc.debian.org](irc.debian.org) and
the [debian-switzerland@lists.debian.org](debian-switzerland@lists.debian.org)
list. For more private information, talk to [board@debian.ch](board@debian.ch).
