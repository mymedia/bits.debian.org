Title: New Debian Developers and Maintainers (September and October 2019)
Slug: new-developers-2019-10
Date: 2019-11-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Translator: 
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Teus Benschop (teusbenschop)
  * Nick Morrott (nickm)
  * Ondřej Kobližek (kobla)
  * Clément Hermann (nodens)
  * Gordon Ball (chronitis)

The following contributors were added as Debian Maintainers in the last two months:

  * Nikos Tsipinakis
  * Joan Lledó
  * Baptiste Beauplat
  * Jianfeng Li

Congratulations!

