Title: DebConf19 procura patrocinadores!
Date: 2019-01-10 18:30
Tags: debconf, debconf19, sponsors
Slug: debconf19-looking-for-sponsors
Author: Laura Arjona Reina, Andre Bianchi and Paulo Santana
Artist: DebConf19 Design Team
Lang: pt-BR
Translator: Andre Bianchi
Status: published

**A [DebConf19](https://debconf19.debconf.org) vai acontecer em
[Curitiba](https://debconf19.debconf.org/about/curitiba/), Brasil, de 21 a 28
de Julho de 2019**. Ela será precedida pelo DebCamp, de 14 a 19 de Julho,
e pelo [Open Day](https://debconf19.debconf.org/schedule/openday/) no dia 20.

[DebConf](https://www.debconf.org), a conferência anual de desenvolvedores
e desenvolvedoras do Debian, é um evento incrível no qual contribuidores de
todo o mundo se encontram para apresentar, discutir e trabalhar em assuntos
relativos ao sistema operacional Debian. É uma ótima oportunidade para conhecer
as pessoas responsáveis pelo sucesso do projeto e para ver uma comunidade
distribuída em ação trabalhando de forma respeitosa e funcional.

O time da DebConf tem como objetivo organizar a Conferência Debian como um
evento auto-sustentável, apesar de seu tamanho e complexidade. As contribuições
financeiras e apoios de indivíduos, empresas e organizações são centrais para
nosso sucesso.

Existem muitas possibilidades diferentes de apoiar a DebConf e estamos em
processo de contactar potenciais patrocinadores ao redor do globo. Se você
conhece alguma organização que poderia se interessar ou querer retribuir de
volta ao mundo do Software Livre e Código Aberto, por favor envie a ela
o [folheto de
patrocínio](https://media.debconf.org/dc19/fundraising/debconf19_sponsorship_brochure_pt_br.pdf)
ou [entre em contato com a equipe de patrocínio](mailto:sponsors@debconf.org)
para nos sugerir possíveis interessados. Se você for uma empresa e quiser
patrocinar, por favor entre em contato conosco através do e-mail
[sponsors@debconf.org](mailto:sponsors@debconf.org).

Vamos trabalhar juntos, como em todos os anos, para construir a melhor DebConf
de todas. Esperamos por vocês em Curitiba!

![DebConf19 logo](|static|/images/800px-Debconf19-horizontal.png)
