Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng bảy và tám 2019)
Slug: new-developers-2019-08
Date: 2019-09-17 17:30
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Keng-Yu Lin (kengyu)
  * Judit Foglszinger (urbec)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Hans van Kranenburg
  * Scarlett Moore

Xin chúc mừng!

